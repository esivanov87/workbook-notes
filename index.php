<!DOCTYPE html>
<html ng-app="WorkBook">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/app.min.css" />
    <title>AngularJS</title>
</head>
<body>

<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-48">
                <h1><i class="fa fa-folder-open"></i> Блокнот</h1>
            </div>
        </div>
    </div>
</header>

<section>
    <div class="container">
        <ng-view></ng-view>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-48">
                &copy; 2016. Приложение &laquo;Блокнот&raquo;
            </div>
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.1/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.1/angular-route.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.1/angular-resource.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.1/angular-messages.min.js"></script>

<script type="text/javascript" src="app/app.js"></script>
<script type="text/javascript" src="app/restapi/restapi.js"></script>
<script type="text/javascript" src="app/main/main.js"></script>
<script type="text/javascript" src="app/notes/notes.js"></script>

</body>
</html>