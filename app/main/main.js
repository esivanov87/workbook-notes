(function(){
    'use strict';

    angular
        .module('WorkBook.main', ['ngRoute'])
        .config(['$routeProvider', configMain])
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject=['$scope', '$rootScope', '$log'];

    function MainCtrl ($scope, $rootScope, $http, $log) {

        $scope.moduleSettings = {
            moduleName : 'main',
            pageTitle : 'Рабочий стол'
        };

    }

    function configMain ($routeProvider) {
        $routeProvider
            .when ('/', {
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl',
            controllerAs: 'vm'
        });
    }

})();