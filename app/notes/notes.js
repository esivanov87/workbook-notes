(function(){
    'use strict';

    angular
        .module('WorkBook.notes', ['ngRoute'])
        .config(['$routeProvider', configMain])
        .controller('NotesListCtrl', NotesListCtrl)
        .controller('NotesEditCtrl', NotesEditCtrl);

    NotesListCtrl.$inject=['$scope', '$rootScope', '$http', '$log', 'restFactory'];
    NotesEditCtrl.$inject=['$scope', '$rootScope', '$http', '$log', '$routeParams', 'restFactory'];

    function NotesListCtrl ($scope, $rootScope, $http, $log, restFactory) {

        var vm = this;

        $scope.moduleSettings = {
            moduleName : 'notes',
            pageTitle : 'Список записей'
        };

        vm.item = {};

        vm.item = restFactory.get({type_request:'request_notes'});

        $log.debug(vm.item);

    }

    function NotesEditCtrl ($scope, $rootScope, $http, $log, $routeParams, restFactory) {

        var vm = this;

        $scope.moduleSettings = {
            moduleName : 'notes',
            pageTitle : 'Редактирование записи',
            itemID : $routeParams.id
        };

        vm.item = {};
        vm.item_type = {};

        vm.item_type = restFactory.get({type_request:'request_type_notes'});

        //$log.debug(vm.item_type);

    }

    function configMain ($routeProvider) {
        $routeProvider
            .when ('/notes', {
                templateUrl: 'app/notes/notes_list.html',
                controller: 'NotesListCtrl',
                controllerAs: 'vm'
            })
            .when ('/notes/:id/edit', {
                templateUrl: 'app/notes/notes_edit.html',
                controller: 'NotesEditCtrl',
                controllerAs: 'vm'
            });
    }

})();