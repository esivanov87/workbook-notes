(function(){
    'use strict';

    angular
        .module('WorkBook', [
            'ngRoute',
            'ngResource',
            'WorkBook.restAPI',
            'WorkBook.main',
            'WorkBook.notes'
        ])
        .constant('SERVER_API', 'http://85.143.220.83')
        .config(cfgMain);

    cfgMain.$inject = ['$routeProvider', '$locationProvider', '$logProvider'];

    function cfgMain ($routeProvider, $locationProvider, $logProvider) {
        $routeProvider
            .otherwise({ redirectTo: '/' });
        //$locationProvider.html5Mode(true);
        $logProvider.debugEnabled(true);
    }

})();