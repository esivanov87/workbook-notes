(function(){
    'use strict';

    angular
        .module('WorkBook.restAPI', ['ngRoute', 'ngResource'])
        .factory('restFactory', ['$resource', 'SERVER_API', function ($resource, SERVER_API) {
            //return $resource(SERVER_API + '/:moduleId/:itemId:paramsQuery');

            /*var resource = $resource(SERVER_API + '/api_demo/:type_request.php');

            this.query = function (){
                return resource;
            }*/

            return $resource(SERVER_API + '/api_demo/:type_request.php',
                {type_request: '@type_request'},
                {
                    query: {
                        method: 'GET',
                        isArray: false
                    },
                    get: {
                        method: 'GET',
                        //cache: true
                    }
                }
            );

            /*return $resource('/server_api.php',
                {id: '@id'},
                {
                    query: {
                        isArray: true,
                        method: 'GET',
                        params: {},
                        transformResponse: function (data) {
                            //return angular.fromJson(data).items
                            return data;
                        }
                    },
                    get: {method: 'GET', params: {id: '@id'}},
                    save: {method: 'POST'},
                    update: {method: 'PUT', params: {id: '@id'}},
                    delete: { method: 'DELETE', params: {} }
                }
            );*/

            /*return $resource('/server_api.php',
                {},
                {
                    query: {
                        method: 'GET',
                        isArray: false,
                        transformResponse: function (data, headers) {
                            //if no data return so no warnings
                            if (data == ''){
                                return;
                            }

                            return {data: $.extend({}, eval("{" + data + '}'))};
                        }
                    }
                }
            );*/


        }])

})();