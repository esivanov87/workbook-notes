<?php

$arData = [
    "count" => '5',
    "items" => [
        [
            "id" => 1,
            "name" => "Текст",
        ],
        [
            "id" => 2,
            "name" => "Ссылка",
        ],
        [
            "id" => 3,
            "name" => "Картинка",
        ],
        [
            "id" => 4,
            "name" => "Видео",
        ],
        [
            "id" => 5,
            "name" => "Событие",
        ],
        [
            "id" => 6,
            "name" => "Встреча",
        ],
    ]
];

/*echo "<pre>";
print_r($arData);
echo "</pre>";*/

header('Content-Type: application/json');
echo json_encode($arData);